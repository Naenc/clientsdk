<?php

namespace GeniusSystems\ClientSdk;

class Greet
{
    public $name;

    public function greet($greet = "Hello World")
    {
        return $greet;
    }

    function set_name($name)
    {
        $this->name = $name;
    }

    function get_name()
    {
        return $this->name;
    }
}

?>